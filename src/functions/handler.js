'use strict';
const { RankingFinder } = require('../models/finder');

module.exports.main = async (event) => {

  try {
    const position = event.pathParameters["position"];
    const language = event.pathParameters["language"];

    const finder = new RankingFinder(position, language);
    const refactoredData = await finder.readData();
    finder.setRefactoredData(refactoredData);
    finder.searchRepositoryByNumber();
    const result = finder.formatOutput();

    return response('The request was succesfully', result, 200);
  } catch (error) {
    return response('There was an error procesing the request', error, 400);
  }

}

function response(message, data, code) {
  return {
    statusCode: code,
    body: JSON.stringify(
      {
        message: message,
        data,
      },
      null,
      2
    ),
  };

}
