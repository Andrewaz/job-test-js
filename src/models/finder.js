const axios = require('axios').default;
const fs = require('fs');

class RankingFinder {
    number;
    language;
    refactoredData = [];
    nTopRepositories = [];
    constructor(_number, _language) {
        this.number = _number;
        this.language = _language;
    }


    async readData() {
        return new Promise((resolve, reject) => {
            let refactoredData = [];
            try {

                fs.readFile('ranking.csv', 'utf8', function (err, data) {
                    const lines = data.split("\n");
                    let skipedTitles = false;
                    let positionCounter = 0;
                    while (typeof lines[0] !== "undefined") {
                        const line = lines.shift();
                        const _dataLine = line.split(',');
                        if (skipedTitles) {
                            refactoredData.push({
                                position: positionCounter, rank: _dataLine[0], item: _dataLine[1], repo_name: _dataLine[2], stars: _dataLine[3], forks: _dataLine[4], language: _dataLine[5],
                                repo_url: _dataLine[6], username: _dataLine[7], issues: _dataLine[8], last_commit: _dataLine[9], description: _dataLine[10]
                            })
                            positionCounter++;
                        }
                        else skipedTitles = true;
                    }
                    resolve(refactoredData);
                });
            } catch (error) {
                reject(error)
            }
        })
    }

    setRefactoredData(_data) {
        this.refactoredData = _data;
    }

    getRankingData() {
        return this.refactoredData;
    }
    getTopRepositories() {
        return this.nTopRepositories;
    }
    searchRepositoryByNumber() {
        const reposByLanguage = this.refactoredData.filter(rep => rep.language === this.language);
        this.nTopRepositories = reposByLanguage.sort((a, b) => { return a.starts - b.starts });
        console.log(this.nTopRepositories[1]);
    }
    formatOutput() {
        let outPut = '';
        this.nTopRepositories.forEach((item, index) => {
            if (index < this.number) {
                delete item.position;
                const value = Object.values(item).join(',')
                outPut = `${outPut}\n ${value}`
            }
        })

        return outPut;
    }


}

module.exports = { RankingFinder }