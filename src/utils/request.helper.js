
function request(event) {
    if (event.httpMethod !== undefined) {
        const request = event;
        const method = request.httpMethod;
        const params = request.queryStringParameters || request.pathParameters;
        return { method, params, body: JSON.parse(request.body) };
    } else {
        const { extensions } = event;
        const { method, query, body } = extensions.request;
        return { method, params: query || {}, body: JSON.parse(body) }
    }

}
module.exports = request;