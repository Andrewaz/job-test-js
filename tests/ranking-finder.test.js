

const init = require('./steps/init');
const { authenticated_aws_user, authenticate_jwt_user, verify_jwt_user } = require('./steps/given');
const { get_ranking_request } = require('./steps/when');
// let idToken;

// describe('Given an AWS authenticated user', () => {
//     beforeAll(async () => {
//         init();
//         let user = await authenticated_aws_user();
//         idToken = user.AuthenticationResult.IdToken;
//     });
//         describe('When we invoke the GET / endpoint', () => {
//             it('Should expect to get N top repositories', async () => {
//                 expect(true).toBe(true);
//             })
//     })
// });


describe('Authenticate JWT user', () => {
    init();
    let token = authenticate_jwt_user();
    console.log('New JWT token:', token);
});


describe('Given an JWT authenticated user', () => {
    init();
    let authenticated_user = verify_jwt_user();
    it('Should expect to a valid and authenticated jwt user', () => {
        expect(authenticated_user).toBe(true);
    });

    it('Should expect to a valid string output', async () => {
        const { message, data } = await get_ranking_request(2, 'JavaScript');
        console.log(data);
        expect(message).toEqual('The request was succesfully');
    });
});