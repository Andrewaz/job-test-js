// test when the user sent any type of request

const axios = require('axios').default;



exports.get_ranking_request = async (number, language) => {
    const response = await axios.get(`http://localhost:3000/dev/validate/${number}/${language}`);
    return response.data;
};

